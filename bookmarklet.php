<?php
$item_id = isset($_GET['item_id']) ? $_GET['item_id'] : '""';
$item_pp = isset($_GET['item_pp']) ? intval($_GET['item_pp']) : '""';
?>
<!DOCTYPE html>
<html>
<head>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="js/storejson2.min.js"></script>
    <script src="js/rsurl.js"></script>

    <script>
        function bookmarklet_init(){
            var item_id =  <?php echo $item_id; ?>;
            var item_pp =  <?php echo $item_pp; ?>;
            if(!item_id) return;
            getItemData({cmd: 'get-item-stats', rid: gen(), id: item_id, pp: item_pp}, true);
        }
    </script>
</head>
<body onload="bookmarklet_init();">

</body>
</html>