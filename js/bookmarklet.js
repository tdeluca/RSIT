var QueryString = function () {
    // This function is anonymous, is executed immediately and
    // the return value is assigned to QueryString!
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = pair[1];
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]], pair[1] ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(pair[1]);
        }
    }
    return query_string;
} ();

var obj = QueryString.obj;
if(window.location.hostname === 'services.runescape.com' && typeof obj != 'undefined'){
    var pp = prompt('Optional: Fill in the purchase price of this item');
    if(pp != null){
        var url = 'http://localhost/rsit/bookmarklet.php?item_id=' + QueryString.obj;
        if (pp != '') url += '&item_pp=' + parseInt(pp, 10);

        var iframe = document.createElement('iframe');
        iframe.src = url;
        iframe.id = 'rsit_iframe';
        iframe.style.display = 'none';

        document.body.appendChild(iframe);
        alert('Item added!');
    }
} else {
    alert('You can only use this on a Runescape item page');
}