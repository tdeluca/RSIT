var items;
var items_index = 0;
function init() {

    $.get('cmd.php', {cmd: 'get-ge-data'}, function(data) {
        $("#data-tabel .ge-update").html(data);
    });

    if(typeof store.get('items') === 'undefined'){
        store.set('items', []);
    }
    items = store.get('items');
    var len = items.length;
    
    if (len > 0) {
        for(var i = 0; i < len; i++){
            getItemData({cmd: 'get-item-stats', rid: items[i].rowId, id: items[i].id, pp: items[i].pp}, false);
        }
    }
    checkEmptyTable();

    //FAQ knop
    $('#faq-button').on('click', function() {
        $('#faq').toggle();
        $(this).toggleClass('button-active');
    });
    
    //item toevoegen
    $("form#add-item").submit(function(){
        var item_id =  $("#search-item-input").val();
        var item_pp =  $("#item-pp").val();
        if(!item_id) return;
        getItemData({cmd: 'get-item-stats', rid: gen(), id: item_id, pp: item_pp}, true);
        $("#item-id, #search-item-input, #item-pp").val("");
        $('#search-results').html('');

    });
    
    //item verwijderen
    $('#data-tabel').on('click', '.delete-item-button', function(){
        var to_delete = parseInt($(this).attr('data-delete'));
        items = store.get('items');
        var newarr = [];
        for(var i in items){
            if(items[i].rowId != to_delete){
                newarr.push(items[i]);
            }
        }
        store.set('items', newarr);
        $("#item-"+to_delete).remove();
        checkEmptyTable();
    });

    //item zoeken
    $('#search-item-input').blur(function(e){
        if($(this).val() == '') return;
        $.get('cmd.php', {cmd: 'search-item', search: $('#search-item-input').val()}, function(data){
            data = JSON.parse(data);
            if(data.status === 'OK') {
                var html = '<img src="' + data.icon + '"> <span>' + data.name + '</span>';
                $('#search-results').html(html);
            } else {
                $('#search-results').html('No item found with this ID');
            }
        });
    });
}

//functie voor het ophalen van data
function getItemData(parameters, new_item) {
    ni = new_item || false;
    $.get('cmd.php', parameters, function(data) {
        $("#data-tabel tbody").append(data);
        if(ni){
            items = store.get('items');
            items.push({rowId: parameters.rid, id: parameters.id, pp: parameters.pp});
            store.set('items', items);
        }
        checkEmptyTable();
    });
}

//functie voor het checken op en lege tabel
function checkEmptyTable(){
    if(store.get('items').length === 0){
        $("#no-item-text").show();
    } else {
        $("#no-item-text").hide();
    }
}

function gen(){
    var myarr = [];
    for(var i in items){
        myarr.push(items[i].rowId);
    }
    rowId = 0;
    if(myarr.length === 0) return rowId;
    for(var i = 0; i <= myarr.length; i++){
        if($.inArray(rowId, myarr) === -1){
            break;
        }
        rowId++;
    }
    return rowId;
}