<?php

include 'ScriptsClass.php';
$sc = new Scripts();

$cmd = $_GET['cmd'];

if ($cmd == 'get-item-stats') {
    $item_rid = $_GET['rid'];
    $item_id = $_GET['id'];
    $item_pp = isset($_GET['pp']) && is_numeric($_GET['pp']) ? intval($_GET['pp']) : 0;

    $data = $sc::getData('http://services.runescape.com/m=itemdb_rs/api/catalogue/detail.json?item=' . $item_id, true);
    $json = array();
    //if the pp is not 0
    if ($item_pp != 0) {
        //get the data
        $sales = $sc::getData('http://services.runescape.com/m=itemdb_rs/api/graph/' . $item_id . '.json', true);
        $daily = array_values($sales['daily']);
        $precise = $daily[count($daily) - 1];

        //calculate and format
        $profit_precise = intval($precise - $item_pp);
        $profit_formatted = number_format($precise - $item_pp);
        $pp_formatted = number_format($item_pp);

        //setting profit_trend for the class
        if ($profit_precise > 0) {
            $profit_trend = 'positive';
        } else if ($profit_precise < 0) {
            $profit_trend = 'negative';
        } else {
            $profit_trend = 'neutral';
        }
    } else {
        //if the pp is 0, set default values
        $profit_formatted = "-";
        $pp_formatted = "-";
        $profit_trend = "neutral";
    }
    //echo the output
    $json['icon'] = $data['item']['icon'];
    $json['id'] = $data['item']['id'];
    $json['name'] = $data['item']['name'];
    $json['current_price'] = $data['item']['current']['price'];
    $json['price_change_trend'] = $data['item']['today']['trend'];
    $json['price_change_price'] = $data['item']['today']['price'];
    $json['purchase_price_formatted'] = $pp_formatted;
    $json['profit_trend'] = $profit_trend;
    $json['profit_formatted'] = $profit_formatted;

    $json['new_item'][0] = $item_id;
    $json['new_item'][1] = $item_pp;
    //echo json_encode($json);
    echo "<tr id='item-$item_rid'>
            <td class='item-icon'><img src='{$data['item']['icon']}'></td>
                <td class='item-name'><a href='http://services.runescape.com/m=itemdb_rs/viewitem.ws?obj={$data['item']['id']}' target='_blank' rel='nofollow'>{$data['item']['name']}</a></td>
                <td class='item-price'>{$data['item']['current']['price']} (<span class='item-change {$data['item']['today']['trend']}'>{$data['item']['today']['price']}</span>)</td>
                <td class='item-pp'>$pp_formatted</td>
                <td class='item-profit $profit_trend'>$profit_formatted</td>
                <td class='item-delete'><button class='pure-button button-xsmall delete-item-button' data-delete='$item_rid'>Delete</button></td>
            </tr>";

    exit;
}

if ($cmd == 'get-ge-data') {
    date_default_timezone_set("Europe/Amsterdam");
    $sales = $sc::getData('http://services.runescape.com/m=itemdb_rs/api/graph/1944.json', true);
    $daily = array_keys($sales['daily']);
    $precise = $daily[count($daily) - 1] / 1000;

    $date = date('d/m/Y', $precise);
    echo "Last Grand Exchange update: $date";
    exit;
}

if($cmd == 'search-item'){
    $item_id = $_GET['search'];
    $data = $sc::getData('http://services.runescape.com/m=itemdb_rs/api/catalogue/detail.json?item=' . $item_id, true);

    if($data){
        $json['status'] = 'OK';
        $json['name'] = $data['item']['name'];
        $json['icon'] = $data['item']['icon'];
    } else {
        $json['status'] = 'error';
    }

    echo json_encode($json);
    exit;
}