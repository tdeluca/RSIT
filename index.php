<!DOCTYPE html>
<html>
    <head>
        <title>RuneScape Item Tracker URL Edition - Thomas de Luca</title>
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="js/storejson2.min.js"></script>
        <script src="js/rsurl.js"></script>
        <!--<script src="http://www.thomasdeluca.nl/files/ga.js"></script>-->

        <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/pure/0.4.2/pure-min.css" />
        <link rel="stylesheet" href="css/flexboxgrid.min.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="shortcut icon" href="http://services.runescape.com/m=itemdb_rs/obj_sprite.gif?id=1944" >
    </head>
    <body onload="init();">

        <div id="wrapper" >
            <div class="row">
                <div class="col-xs-12">
                    <h1>RuneScape Item Tracker</h1>
                    <span>Made by <a href="http://www.thomasdeluca.nl/portfolio/runescape-item-tracker/" target="_blank">Thomas de Luca</a></span>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div id="tabel-div">
                        <table class="pure-table pure-table-horizontal" id="data-tabel">

                            <thead>
                            <tr><th class="ge-update" data-sortable="false" colspan="6">Loading..</th></tr>
                            <tr>
                                <th data-sortable="false">Icon</th>
                                <th >Name</th>
                                <th>Price today</th>
                                <th>Purchase price</th>
                                <th>Profit</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr id='no-item-text'><td colspan='6' style='text-align: center;'>To add an item, fill in the item name or item ID and (optionally) the purchase price.</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <form action="javascript:void(0);" class="pure-form" id="add-item">
                        <input type="text" name="search-item-input" id="search-item-input" placeholder="ID of item to add"  value="" />
                        <div id="search-results"></div>
                        <p><input type="number" name="item-pp" id="item-pp" placeholder="Purchase Price" /></p>
                        <p><button class="pure-button pure-button-primary" type="submit">Add item</button></p>
                    </form>
                </div>
            </div>



            <hr class="divider" />

            <h2 class="pure-button pure-button-primary" id="faq-button">Frequently Asked Questions</h2>
            <div id="faq" style="display:none;">
                <div class="row">
                    <div class="col-xs-6">
                        <span><b>Usage</b></span>
                        <ol>
                            <li>Search an item</li>
                            <li>Select your item from search results</li>
                            <li>Optionally fill in other fields</li>
                            <li>Click on 'Add item'</li>
                        </ol>
                    </div>
                    <div class="col-xs-6">
                        <span><b>Huh? No login?</b></span>
                        <p>It stores data on your computer, instead of the cloud. Upside: No login!<br />Downside: These items only appear on your computer.</p>
                    </div>
                    <div class="col-xs-6">
                        <span><b>Add item bookmarklet</b></span>
                        <p>Drag and drop the link below in your bookmarks bar. Navigate to an item page on the Runescape website <a href="http://services.runescape.com/m=itemdb_rs/Egg/viewitem.ws?obj=1944" target="_blank">like this one</a> and click the bookmarklet. Add items on-the-go!</p>
                        <h1><a href="javascript:void((function(d)%7Bvar e=d.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://localhost/rsit/js/bookmarklet.js');d.body.appendChild(e)%7D)(document));">Add this item!</a></h1>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

