<?php 

    class Scripts {
        
        public static function getData($url, $decode = false){
            $ch = curl_init($url); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            $data = curl_exec($ch);
            curl_close($ch);
            if($decode) return json_decode($data, true);
            return $data;
        }
        
        public static function debug($var, $exit = false){
            echo "<pre>";
            print_r($var);
            echo "</pre>";
            if($exit) exit;
        }
        
        public static function toTable($arr){
            echo "<table border='1'>";
            foreach($arr as $row){
                echo "<tr>";
                foreach($row as $cell){
                    echo "<td>$cell</td>";
                }
                echo "</tr>";
            }
            echo "</table>";
        }
    }